export default {
    title: 'GLIDE',
    url: {
        home: '/',
        employee: '/employee'
    },
    back_top: 'Back To Top',
    employees: (qty) => `${qty} ${qty > 1 ? 'employees' : 'employee'}`,
    employees_array: [
        {
            id: 1,
            first:'firstname',
            last:'lastname',
            manager:0
        },
        {
            id: 2,
            first:'firstname',
            last:'lastname',
            manager:0
        },
        {
            id: 3,
            first:'firstname',
            last:'lastname',
            manager:0
        },
        {
            id: 4,
            first:'firstname',
            last:'lastname',
            manager:0
        },
        {
            id: 5,
            first:'firstname',
            last:'lastname',
            manager:1
        },
        {
            id: 6,
            first:'firstname',
            last:'lastname',
            manager:1
        },
        {
            id: 7,
            first:'firstname',
            last:'lastname',
            manager:1
        },
        {
            id: 8,
            first:'firstname',
            last:'lastname',
            manager:1
        },
        {
            id: 9,
            first:'firstname',
            last:'lastname',
            manager:1
        },
        {
            id: 10,
            first:'firstname',
            last:'lastname',
            manager:1
        },
        {
            id: 11,
            first:'firstname',
            last:'lastname',
            manager:1
        },
        {
            id: 12,
            first:'firstname',
            last:'lastname',
            manager:1
        },
        {
            id: 13,
            first:'firstname',
            last:'lastname',
            manager:1
        },
        {
            id: 14,
            first:'firstname',
            last:'lastname',
            manager:1
        },
        {
            id: 15,
            first:'firstname',
            last:'lastname',
            manager:5
        }
    ]
}