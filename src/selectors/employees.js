export const getEmployees = state =>  state.employees.employees
export const getTopManagers = state =>  state.employees.employees.filter(item => item.manager === 0)
export const getEmployeesPending = state => state.employees.pending
export const getEmployeesError = state => state.employees.error