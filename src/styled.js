import styled, {css} from 'styled-components'
import mediaQueries from './utils/media-queries'

export const Wrapper = styled.div`
    padding: 30px 40px 0;
    background-image: linear-gradient(${({theme}) => `${theme.colors.gray_light}, ${theme.colors.white}`});
`

export const HeaderContainer = styled.div`
    width: 100%;
    height: 100px;
    ${mediaQueries.mobile`
        height: 100px;
    `};   
    ${({center}) =>
    center &&
    css`
        display: flex;
        align-items: center;
        justify-content: center;    
    `};
`

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`