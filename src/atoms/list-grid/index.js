import React from 'react'
import {Grid, Item} from './styled'
import PropTypes from 'prop-types'

const ListGrid = ({items, noWrap}) =>
    <Grid noWrap={noWrap}>
        {items.map((item, index) =>
            <Item key={index}>
                {item}
            </Item>
        )}
    </Grid>

ListGrid.propTypes = {
    noWrap: PropTypes.bool,
    items: PropTypes.array.isRequired
}

export default ListGrid
