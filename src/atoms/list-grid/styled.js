import styled from 'styled-components'
import mediaQueries from '../../utils/media-queries'

export const Grid = styled.div`
    display: flex;
    flex-wrap: ${({noWrap}) => noWrap ? 'nowrap': 'wrap'};
    width: 100%;
    justify-content: space-between;
    align-items: stretch;
    height: 100%;
    ${mediaQueries.small`
      flex-direction: column;
      align-items: center;
    `};
`
export const Item = styled.div`
    margin-bottom: 40px;
    margin: 20px;
`