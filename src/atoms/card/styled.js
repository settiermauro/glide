import styled, { css } from 'styled-components'

export const Container = styled.div`
    border-radius: 10px;
    width: 100%;
    height: 100%;
    background-color: ${({theme}) => theme.colors.white};
    ${({onClick, disabled}) =>
    onClick && !disabled &&
        css`
        cursor: pointer;
    `};
    ${({ disabled, theme }) =>
    disabled ? 
    css`
      border: solid 1px ${theme.colors.gray_dark};
      background-color: ${theme.colors.gray_light};
    ` : css`
      box-shadow: 0 2px 10px 0 ${theme.shadow.black25}, 0 2px 5px 0 ${theme.shadow.black10};
    `};
`