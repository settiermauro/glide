import React from 'react'
import {Container} from './styled'
import PropTypes from 'prop-types'

const Card = ({children, disabled, onClick}) =>
    <Container disabled={disabled} onClick={onClick}>
        {children}
    </Container>

Card.propTypes = {
    disabled: PropTypes.bool,
    onClick: PropTypes.func
}

export default Card
