import styled from 'styled-components'

export const StyledButton = styled.button`
    display: flex;
    align-items: center;
    color: ${({ theme }) => theme.colors.gray_dark};
    border-radius: 25px;
    border: none;
    padding: 5px 10px;
    cursor: pointer;
    background-color: ${({ theme }) => theme.colors.white};
    font-size: 15px;
    ${({ theme }) =>
    `
      box-shadow: 0 2px 10px 0 ${theme.shadow.black25}, 0 2px 5px 0 ${theme.shadow.black10};
    `};
`

export const IconWrapper = styled.div`
    padding-right: 5px;
    justify-content: center;
    color: ${({ theme }) => theme.colors.black};
`