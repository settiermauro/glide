import React from 'react'
import { StyledButton, IconWrapper } from './styled'
import PropTypes from 'prop-types'

const Button = ({onClick, icon, children}) => (
    <StyledButton onClick={onClick}>
        {icon && <IconWrapper>{icon}</IconWrapper>}
        {children}
    </StyledButton>
)

Button.propTypes = {
    icon: PropTypes.element,
    onClick: PropTypes.func
}

export default Button
