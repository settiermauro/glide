import React from 'react'
import { StyledText } from './styled'
import PropTypes from 'prop-types'

const Text = ({big, children, small, xLarge}) => (
    <StyledText xLarge={xLarge} big={big} small={small}>
        {children}
    </StyledText>
)

Text.propTypes = {
    big: PropTypes.bool,
    small: PropTypes.bool,
    xLarge: PropTypes.bool,
    children: PropTypes.node.isRequired
}

export default Text
