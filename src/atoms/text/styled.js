import styled, { css } from 'styled-components'

export const StyledText = styled.span`
    color: ${({ theme }) => theme.colors.gray_dark};
    font-size: 15px;
      
    ${({ big }) =>
      big &&
      css`
        font-weight: Bold;
        font-size: 18px;
    `};
      
    ${({small}) =>
    small &&
    css`
        font-weight: Bold;
        font-size: 12px;
    `};
      
    ${({xLarge}) =>
    xLarge &&
    css`
        font-weight: Bold;
        font-size: 24px;
    `};
`