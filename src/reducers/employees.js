import {FETCH_EMPLOYEES_ERROR, FETCH_EMPLOYEES_PENDING, FETCH_EMPLOYEES_SUCCESS} from '../actions/employees'
import initial_state from './initial_state'

const default_state = initial_state.employees

export const employees = (state = default_state, action) => {
    switch(action.type) {
        case FETCH_EMPLOYEES_PENDING:
            return {
                ...state,
                pending: true,
                error: null
            }
        case FETCH_EMPLOYEES_SUCCESS:
            let output = state.employees
            const new_employees = action.employees
            new_employees.forEach(elem => {
                if(!output.find(item => item.id === elem.id)){
                    output.push(elem)
                }
            })
            return {
                ...state,
                pending: false,
                employees: output.slice()
            }
        case FETCH_EMPLOYEES_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default:
            return state
    }
}