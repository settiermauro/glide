import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {fetchEmployees} from '../../actions/employees'
import {getTopManagers, getEmployeesError, getEmployeesPending} from '../../selectors/employees'
import {CenterStrip} from './styled'
import {HeaderContainer, Container} from '../../styled'
import ListGrid from '../../atoms/list-grid'
import Text from '../../atoms/text'
import {withRouter} from 'react-router-dom'
import EmployeeCardContainer from '../employee-card'
import constants from '../../utils/constans'

class Home extends Component{

    componentDidMount() {
        const {fetchEmployees} = this.props
        fetchEmployees({manager:0})
    }

    goEmployee = (id) => {
        const { history } = this.props
        history.push(`${constants.url.employee}/${id}`)
    }

    render() {
        const { employees } = this.props

        return (
            <Container>
                <HeaderContainer
                    center>
                    <Text
                        xLarge>
                        {constants.title}
                    </Text>
                </HeaderContainer>
                <CenterStrip>
                    <ListGrid
                        items={employees.map(employee =>
                            <EmployeeCardContainer
                                goEmployee={this.goEmployee}
                                employee={employee}
                            />)}/>
                </CenterStrip>
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    error: getEmployeesError(state),
    pending: getEmployeesPending(state),
    employees: getTopManagers(state)
})

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchEmployees
}, dispatch)

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(Home)
)