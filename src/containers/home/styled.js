import styled from 'styled-components'
import mediaQueries from '../../utils/media-queries'

export const CenterStrip = styled.div`
    width: 100%;
    height: 150px;
    ${mediaQueries.mobile`
        height: auto;
    `};
`