import React, {Component} from 'react'
import EmployeeCard from '../../organisms/employee-card'
import {connect} from 'react-redux'
import {getEmployees, getEmployeesError, getEmployeesPending} from '../../selectors/employees'
import {bindActionCreators} from 'redux'
import {fetchEmployees} from '../../actions/employees'

class EmployeeCardContainer extends Component{

    componentDidMount() {
        const {employee, fetchEmployees, dontLoad} = this.props
        if(!dontLoad) {
            fetchEmployees({manager: employee.id})
        }
    }

    render() {
        const {employee, employees, goEmployee, selected, boss} = this.props
        const qty = employees.filter(elem => elem.manager === employee.id).length
        return (
            <EmployeeCard
                boss={boss}
                selected={selected}
                first={employee.first}
                last={employee.last}
                employeesQty={qty}
                onClick={qty && goEmployee ? () => goEmployee(employee.id) : null} />
        )
    }
}

const mapStateToProps = state => ({
    error: getEmployeesError(state),
    pending: getEmployeesPending(state),
    employees: getEmployees(state)
})

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchEmployees
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EmployeeCardContainer)