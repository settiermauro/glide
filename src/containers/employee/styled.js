import styled from 'styled-components'

export const ListContainer = styled.div`
    margin-top: 60px;
    width: 100%;
    border-radius: 10px;
    ${({ theme }) =>
    `
      box-shadow: inset 0px 0px 10px ${theme.shadow.black10};
      background-color: ${theme.colors.gray_light};
    `};
`