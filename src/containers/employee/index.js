import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {fetchEmployees} from '../../actions/employees'
import {getEmployeesError, getEmployeesPending, getEmployees} from '../../selectors/employees'
import Header from '../../molecules/header'
import EmployeeCardContainer from '../employee-card'
import {ListContainer} from './styled'
import {HeaderContainer, Container} from '../../styled'
import {withRouter} from 'react-router-dom'
import constants from '../../utils/constans'
import Loader from 'react-loader-spinner'
import theme from '../../theme'
import ListGrid from '../../atoms/list-grid'

class Employee extends Component{

    componentDidMount() {
        const {fetchEmployees, match, employees} = this.props
        const id = parseInt(match.params.id)
        const employee = employees.find(item => item.id === id)
        let filter = {manager:id}
        if(!employee){
            filter.id = id
        }
        fetchEmployees(filter)
    }

    componentDidUpdate() {
        const {fetchEmployees, match, employees} = this.props
        const id = parseInt(match.params.id)
        const employee = employees.find(item => item.id === id)
        if(employee && employee.manager!==0){
            const boss = employees.find(item => item.id === employee.manager)
            if(!boss) {
                fetchEmployees({id: employee.manager})
            }
        }
    }

    goBack = () => {
        const {history} = this.props
        history.push(constants.url.home)
    }

    goEmployee = (id) => {
        const { history } = this.props
        history.push(`${constants.url.employee}/${id}`)
    }

    render() {
        const { employees, match } = this.props
        const employee = employees.find(item => item.id === parseInt(match.params.id))
        const boss = employee && employees.find(item => item.id === employee.manager)
        const employees_show = employee ? employees.filter(item => item.manager === employee.id) : []

        return (
            <Container>
                <HeaderContainer>
                    <Header onClick={this.goBack}>
                        {!employee || (employee && employee.manager !== 0) && (
                            !boss ?
                                <Loader
                                    type={'Grid'}
                                    color={theme.colors.gray_dark}/>
                                :
                                <EmployeeCardContainer
                                    boss
                                    employee={boss}
                                    goEmployee={this.goEmployee}/>
                        )
                        }
                    </Header>
                </HeaderContainer>
                {!employee ?
                    <Loader
                        type={'Grid'}
                        color={theme.colors.gray_dark}/>
                    :
                    <EmployeeCardContainer
                        selected
                        dontLoad
                        employee={employee} />
                }
                <ListContainer>
                    <ListGrid
                        items={employees_show.map(employee =>
                            <EmployeeCardContainer
                                goEmployee={this.goEmployee}
                                employee={employee}
                            />)}/>
                </ListContainer>
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    error: getEmployeesError(state),
    pending: getEmployeesPending(state),
    employees: getEmployees(state)
})

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchEmployees
}, dispatch)

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(Employee)
)