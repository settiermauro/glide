import React from 'react'
import Card from '../../atoms/card'
import Text from '../../atoms/text'
import {Container, Wrapper} from './styled'
import constants from '../../utils/constans'

const EmployeeCard = ({first, last, employeesQty, onClick, selected, boss}) =>
    <Container big={selected} boss={boss}>
        <Card disabled={(employeesQty === 0 || !onClick) && !selected} onClick={onClick}>
            <Wrapper>
                <Text big={!boss}>
                    {`${first} ${last}`}
                </Text>
                {employeesQty > 0 &&
                <Text small={boss}>
                    {`(${constants.employees(employeesQty)})`}
                </Text>
                }
            </Wrapper>
        </Card>
    </Container>

export default EmployeeCard
