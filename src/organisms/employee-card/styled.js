import styled, {css} from 'styled-components'

export const Container = styled.div`
    width: 175px;
    height: 80px;
    ${({ big }) =>
    big &&
    css`
        width: 200px;
        height: 150px;
    `};
    ${({boss}) =>
    boss &&
    css`
        width: 150px;
        height: 60px;
    `};
`
export const Wrapper = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`