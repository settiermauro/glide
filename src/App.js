import React from 'react'
import {applyMiddleware, createStore} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import Home from './containers/home'
import Employee from './containers/employee'
import rootReducer from './reducers'
import initial_state from './reducers/initial_state'
import {ThemeProvider} from 'styled-components'
import theme from './theme'
import {Wrapper} from './styled'
import {BrowserRouter as Router, Route} from 'react-router-dom'

const middlewares = [thunk]

const store = createStore(rootReducer, initial_state, applyMiddleware(...middlewares))

function App() {
    return (
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <Router>
                    <Wrapper>
                        <Route path="/" exact component={Home} />
                        <Route path="/employee/:id" exact component={Employee} />
                    </Wrapper>
                </Router>
            </ThemeProvider>
        </Provider>
    )
}

export default App
