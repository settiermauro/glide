import constants from '../utils/constans'
export const FETCH_EMPLOYEES_PENDING = 'FETCH_EMPLOYEES_PENDING'
export const FETCH_EMPLOYEES_SUCCESS = 'FETCH_EMPLOYEES_SUCCESS'
export const FETCH_EMPLOYEES_ERROR = 'FETCH_EMPLOYEES_ERROR'

const fetchEmployeesPending = () => ({
    type: FETCH_EMPLOYEES_PENDING
})

const fetchEmployeesSuccess = (employees) => ({
    type: FETCH_EMPLOYEES_SUCCESS,
    employees: employees
})

const fetchEmployeesError = (error) => ({
    type: FETCH_EMPLOYEES_ERROR,
    error: error
})

export const fetchEmployees = (filter) =>
    dispatch => {
        dispatch(fetchEmployeesPending())
        let output = []
        Object.keys(filter).forEach(key => {
            output = output.concat(constants.employees_array.filter(elem => elem[key] === filter[key]))
        })
        dispatch(fetchEmployeesSuccess(output))
        /*
        fetch('https://2jdg5klzl0.execute-api.us-west-1.amazonaws.com/default/EmployeesChart-Api')
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw(res.error)
                }
                dispatch(fetchEmployeesSuccess(res))
                return res
            })
            .catch(error => {
                dispatch(fetchEmployeesError(error))
            })*/
    }