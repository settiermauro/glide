import React from 'react'
import { HeaderContainer } from './styled'
import Text from '../../atoms/text'
import Button from '../../atoms/button'
import constants from '../../utils/constans'
import { TiArrowBackOutline } from 'react-icons/ti'
import PropTypes from 'prop-types'

const Header = ({onClick, children}) => (
    <HeaderContainer>
        <Button
            icon={<TiArrowBackOutline />}
            onClick={onClick}>
            <Text>
                {constants.back_top}
            </Text>
        </Button>
        {children}
    </HeaderContainer>
)

Header.propTypes = {
    onClick: PropTypes.func
}

export default Header
